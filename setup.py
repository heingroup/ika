from setuptools import setup, find_packages


setup(
    name='ika',
    version='2.0.1',
    author='Veronica Lai',
    description='Unofficial Python package to control IKA products; not affiliated with IKA.',
    long_description=open("README.md", "r").read(),
    long_description_content_type="text/markdown",
    url='https://gitlab.com/heingroup/ika',
    packages=find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
    ],
    python_requires='>=3.6',
    install_requires=[
        'pyserial'
    ],
)
